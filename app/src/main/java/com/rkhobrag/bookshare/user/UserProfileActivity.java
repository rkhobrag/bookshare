package com.rkhobrag.bookshare.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rkhobrag.bookshare.AddBookActivity;
import com.rkhobrag.bookshare.MainActivity;
import com.rkhobrag.bookshare.R;
import com.rkhobrag.bookshare.Util.BookListAdapter;
import com.rkhobrag.bookshare.book.Book;

import java.util.ArrayList;
import java.util.List;

public class UserProfileActivity extends AppCompatActivity {
    List<Book> bookList = new ArrayList<>();
    private BookListAdapter arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        arrayAdapter = new BookListAdapter(this, bookList);
        ListView listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(arrayAdapter);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userRef = database.getReference("user");
        userRef.orderByChild(MainActivity.EMAILID).equalTo(getIntent().getStringExtra(MainActivity.EMAILID)).getRef().addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists())
                {
                    getSharedBooks(dataSnapshot.getValue(UserProfile.class));
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        FloatingActionButton button = (FloatingActionButton) findViewById(R.id.add);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewBookToProfile();
            }
        });
    }

    private void getSharedBooks(UserProfile userProfile) {
        if(userProfile != null)
        {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference ref = firebaseDatabase.getReference("book");
            for(String key : userProfile.getSharedBooks())
            {
                ref.orderByKey().equalTo(key).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists())
                        {
                            Book value = dataSnapshot.getChildren().iterator().next().getValue(Book.class);
                            bookList.add(value);
                            arrayAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    public void addNewBookToProfile()
    {
        Intent intent = new Intent(this, AddBookActivity.class);
        intent.putExtra(MainActivity.EMAILID, getIntent().getStringExtra(MainActivity.EMAILID));
        startActivity(intent);
    }

}
