package com.rkhobrag.bookshare.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rakesh on 1/6/16.
 */
public class UserProfile {
    String emailId;
    String profilePicLink;
    int contact;
    List<String> sharedBooks = new ArrayList<>();
    String rating;

    @Override
    public String toString() {
        return emailId;
    }

    public UserProfile() {
    }

    public UserProfile(String emailId) {
        this.emailId = emailId;
    }

    public Map<String, Object> toMap()
    {
        Map<String, Object> map = new HashMap<>();
        map.put("emailId", emailId);
        map.put("profilePicLink", profilePicLink);
        map.put("contact", contact);
        map.put("sharedBooks", sharedBooks);
        map.put("rating", rating);
        return map;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getProfilePicLink() {
        return profilePicLink;
    }

    public void setProfilePicLink(String profilePicLink) {
        this.profilePicLink = profilePicLink;
    }

    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    public List<String> getSharedBooks() {
        return sharedBooks;
    }

    public void setSharedBooks(List<String> sharedBooks) {
        this.sharedBooks = sharedBooks;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void addNewSharedBookToUserProfile(String key)
    {
        sharedBooks.add(key);
    }


}
