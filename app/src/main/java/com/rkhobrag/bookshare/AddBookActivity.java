package com.rkhobrag.bookshare;

import android.annotation.TargetApi;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rkhobrag.bookshare.Util.BookFilterAdapter;
import com.rkhobrag.bookshare.book.Book;
import com.rkhobrag.bookshare.common.BookShareException;
import com.rkhobrag.bookshare.user.UserProfile;
import com.rkhobrag.bookshare.user.UserProfileActivity;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A login screen that offers login via email/password.
 */
public class AddBookActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final int REQUEST_READ_CONTACTS = 0;

    private Book book;

    private final List<String> genreList = new ArrayList<>();
    {genreList.add("Horror");genreList.add("Comedy");genreList.add("Thriller");genreList.add("Mystery");genreList.add("Sci-Fi");}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);
        setupActionBar();

        final MultiAutoCompleteTextView genreView = (MultiAutoCompleteTextView)findViewById(R.id.genre);
        genreView.setAdapter(new ArrayAdapter(this, R.layout.users_view, genreList));
        genreView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        final TextView authorView = (TextView)findViewById(R.id.author);

        final List<Book> bookSuggestion = new ArrayList<>();
        final AutoCompleteTextView titleView = (AutoCompleteTextView) findViewById(R.id.book_title);
        titleView.setThreshold(4);
        final BookFilterAdapter adapter = new BookFilterAdapter(this, R.layout.users_view, bookSuggestion, this);
        titleView.setAdapter(adapter);
        titleView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                book = (Book)adapter.getItem(position);
                System.out.println("=======================================\n"+book.toMap());
                genreView.setEnabled(false);
                authorView.setEnabled(false);
            }
        });

        titleView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                genreView.setEnabled(true);
                authorView.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Button button = (Button)findViewById(R.id.add_book_button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(null == book)
                {
                    throw new BookShareException("No Book selected. Please serach for a book title.");
                }

                final View loadingView = findViewById(R.id.share_progress);
                loadingView.setVisibility(View.VISIBLE);
                final String emailId = getIntent().getStringExtra(MainActivity.EMAILID);

                addBook(book, emailId);

                loadingView.setVisibility(View.GONE);
                Intent intent = new Intent(getApplicationContext(), UserProfileActivity.class);
                intent.putExtra(MainActivity.EMAILID, emailId);
                startActivity(intent);
            }
        });
    }

    private void addBook(final Book book, final String emailId) {

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("book");
        ref.orderByChild(Book.TITLE).equalTo(book.getTitle()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    Book book = dataSnapshot.getChildren().iterator().next().getValue(Book.class);
                    book.addNewUserToSharedByList(emailId);
                    Map<String, Object> map = new HashMap<String, Object>();
                    String key = dataSnapshot.getChildren().iterator().next().getKey();
                    map.put(key, book.toMap());
                    ref.updateChildren(map);
                    addBookKeyToUserProfile(emailId, key);
                }
                else
                {
                    String key = ref.push().getKey();
                    Map<String, Object> bookMap = new HashMap<>();
                    book.addNewUserToSharedByList(emailId);
                    bookMap.put(key, book.toMap());
                    ref.updateChildren(bookMap);
                    addBookKeyToUserProfile(emailId, key);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void addBookKeyToUserProfile(final String emailId, final String key) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("user");
        ref.orderByChild(MainActivity.EMAILID).equalTo(emailId).addListenerForSingleValueEvent(new  ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    UserProfile userProfile = dataSnapshot.getChildren().iterator().next().getValue(UserProfile.class);
                    if(userProfile != null ) {
                        userProfile.addNewSharedBookToUserProfile(key);
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put(dataSnapshot.getChildren().iterator().next().getKey(), userProfile.toMap());
                        ref.updateChildren(map);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }



    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
}

