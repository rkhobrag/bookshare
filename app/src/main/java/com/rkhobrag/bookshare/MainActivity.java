package com.rkhobrag.bookshare;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rkhobrag.bookshare.book.SharedBooksActivity;
import com.rkhobrag.bookshare.user.LoginActivity;
import com.rkhobrag.bookshare.user.UserProfile;
import com.rkhobrag.bookshare.user.UserProfileActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String EMAILID = "emailId";
    List<UserProfile> users = new ArrayList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.users_view, users);
        ListView listView = (ListView)findViewById(R.id.user_list);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoUserProfile(users.get(position));
            }
        });

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                users.add(dataSnapshot.getValue(UserProfile.class));
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userRef = database.getReference("user");
        userRef.addChildEventListener(childEventListener);

    }

    public void gotoUserProfile(UserProfile userProfile)
    {
        Intent intent = new Intent(this, UserProfileActivity.class);
        String emailId = userProfile.getEmailId();
        intent.putExtra(EMAILID, emailId);
        startActivity(intent);

    }
    public void gotoMyBooks(View view)
    {
        Intent intent = new Intent(this, MyBooksList.class);
        String username = "rkhobrag";
        intent.putExtra(EMAILID, username);
        startActivity(intent);

    }

    public void gotoBooks(View view)
    {
        Intent intent = new Intent(this, SharedBooksActivity.class);
        String username = "rkhobrag";
        intent.putExtra(EMAILID, username);
        startActivity(intent);
    }

    public void register(View view)
    {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
