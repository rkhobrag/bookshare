package com.rkhobrag.bookshare.Util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.rkhobrag.bookshare.R;
import com.rkhobrag.bookshare.book.Book;
import com.rkhobrag.bookshare.common.BookShareException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.BackingStoreException;

/**
 * Created by rakesh on 14/6/16.
 */
public class BookFilterAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private Activity activity;
    private List<Book> bookList;


    public BookFilterAdapter(Context context, int resource, List objects, Activity activity) {
        super(context, resource, objects);
        this.activity = activity;
        this.bookList = objects;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                if (null != constraint && constraint.toString().length() > 2) {
                    RequestQueue queue = Volley.newRequestQueue(getContext());
                    String url = "https://www.googleapis.com/books/v1/volumes?maxResults=5&q=" + getInputString(constraint);

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(JsonObjectRequest.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                bookList.clear();
                                JSONArray items = response.getJSONArray("items");
                                for (int i = 0; i < items.length(); i++) {

                                    JSONObject jsonObject = items.getJSONObject(i);
                                    JSONObject volumeInfo = getJSONField(jsonObject, "volumeInfo", JSONObject.class);
                                    JSONArray authorsArray = getJSONField(volumeInfo, "authors", JSONArray.class);
                                    List<String> authors = new ArrayList<String>();
                                    if(null != authorsArray)
                                    {
                                        for (int j = 0; j < authorsArray.length(); j++) {
                                            authors.add(authorsArray.get(j).toString());
                                        }
                                    }
                                    JSONArray genresArray = getJSONField(volumeInfo, "categories", JSONArray.class);
                                    List<String> genres = new ArrayList();
                                    if(null != genresArray)
                                    {
                                        for (int j = 0; j < genresArray.length(); j++) {
                                            genres.add(genresArray.get(j).toString());
                                        }
                                    }
                                    Double rating = getJSONField(volumeInfo, "averageRating", Double.class);
                                    JSONObject imageLinks = getJSONField(volumeInfo, "imageLinks", JSONObject.class);
                                    bookList.add(new Book(volumeInfo.getString("title"), authors, genres,
                                            imageLinks == null ?  "" : imageLinks.getString("smallThumbnail"), rating != null ? rating.toString() : "-"
                                    ));
                                    filterResults.count = bookList.size();
                                    filterResults.values = bookList;
                                    notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("====================================================================================" + error);
                        }
                    });
                    queue.add(jsonObjectRequest);
                }
                else
                {
                    filterResults.count = bookList.size();
                    filterResults.values = bookList;
                }
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            }
        };
    }

    private String getInputString(CharSequence constraint) {
        String input;
        try {
            input = URLEncoder.encode(constraint.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new BookShareException("Cannot process input title", e);
        }
        return input;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView rating = (TextView) convertView.findViewById(R.id.rating);
        TextView genre = (TextView) convertView.findViewById(R.id.genre);
        TextView authors = (TextView) convertView.findViewById(R.id.author);

        // getting movie data for the row
        Book m = bookList.get(position);

        // thumbnail image
        thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);

        // title
        title.setText(m.getTitle());

        // genre
        String genreStr = "";
        if(m.getGenre() != null)
        {
            for (String str : m.getGenre()) {
                genreStr += str + ", ";
            }
        }
        genreStr = genreStr.length() > 0 ? genreStr.substring(0,
                genreStr.length() - 2) : genreStr;
        genre.setText(genreStr);

        authors.setText(m.getAuthor().toString());

        rating.setText(m.getRating());

        return convertView;
    }


    private <T> T getJSONField(JSONObject object, String name, Class<T> objectType)
    {
        try{
            return (T)object.get(name);
        } catch (JSONException e) {
            return null;
        }
    }

    public void clearAndUpdate(List<Book> books)
    {
        this.bookList = books;
    }
}
