package com.rkhobrag.bookshare.book;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rkhobrag.bookshare.MainActivity;
import com.rkhobrag.bookshare.R;
import com.rkhobrag.bookshare.Util.AppController;
import com.rkhobrag.bookshare.user.UserProfile;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class BookActivity extends AppCompatActivity {
    private List<UserProfile> sharedBy = new ArrayList();
    ArrayAdapter userListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NetworkImageView thumbNail = (NetworkImageView)findViewById(R.id.thumbnail);
        TextView title = (TextView) findViewById(R.id.title);
        TextView rating = (TextView) findViewById(R.id.rating);
        TextView genre = (TextView) findViewById(R.id.genre);
        TextView authors = (TextView) findViewById(R.id.author);

        Book book = (Book)getIntent().getSerializableExtra("book");

        thumbNail.setImageUrl(book.getThumbnailUrl(), AppController.getInstance().getImageLoader());
        title.setText(book.getTitle());
        rating.setText(book.getRating());
        genre.setText(StringUtils.join(book.getGenre(), ","));
        authors.setText(StringUtils.join(book.getAuthor(), ","));


        ListView userListView = (ListView)findViewById(R.id.user_list);
        userListAdapter = new ArrayAdapter(this, R.layout.users_view, sharedBy);
        userListView.setAdapter(userListAdapter);
        for(String userId : book.getSharedBy())
        {
            updateSharedByList(userId);
        }

    }

    private void updateSharedByList(String userId) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference userDBRef = firebaseDatabase.getReference("user");
        userDBRef.orderByChild(MainActivity.EMAILID).equalTo(userId).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                UserProfile user = dataSnapshot.getValue(UserProfile.class);
                if(null != user)
                {
                    sharedBy.add(user);
                    if(userListAdapter != null)userListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }
}
