package com.rkhobrag.bookshare.book;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rakesh on 5/6/16.
 */
public class Book implements Serializable{
    public static final String TITLE = "title";

    String title;
    List<String> author;
    String type;
    String thumbnailUrl;
    List<String> genre;
    List<String> sharedBy = new ArrayList<>();
    String rating;

    public Book(String title, List<String> author, List<String> sharedBy) {
        this.title = title;
        this.author = author;
        this.sharedBy = sharedBy;
        this.thumbnailUrl = thumbnailUrl;
    }

    public Book(String title, List<String> author, List<String> genre, String thumbnailUrl, String rating) {
        this.title = title;
        this.author = author;
        this.rating = rating;
        this.genre = genre;
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Book() {
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthor() {
        return author;
    }

    public void setAuthor(List<String> author) {
        this.author = author;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public List<String> getGenre() {
        return genre;

    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getSharedBy() {
        return sharedBy;
    }

    public void setSharedBy(List<String> sharedBy) {
        this.sharedBy = sharedBy;
    }

    public void addNewUserToSharedByList(String emailId)
    {
        sharedBy.add(emailId);
    }

    public Map<String, Object> toMap()
    {
        Map<String, Object> map = new HashMap<>();
        map.put("title", title);
        map.put("author", author);
        map.put("type", type);
        map.put("sharedBy", sharedBy);
        map.put("thumbnailUrl", thumbnailUrl);
        map.put("genre", genre);
        map.put("rating", rating);
        return map;
    }

    @Override
    public String toString() {
        return title;
    }
}
