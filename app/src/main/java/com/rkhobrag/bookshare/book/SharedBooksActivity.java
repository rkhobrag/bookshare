package com.rkhobrag.bookshare.book;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rkhobrag.bookshare.R;
import com.rkhobrag.bookshare.Util.BookListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rakesh on 3/7/16.
 */
public class SharedBooksActivity extends AppCompatActivity {
    List<Book> sharedBooks = new ArrayList<>();
    BookListAdapter bookListAdapter;
    String lastItemKey = "";
    String prevLastItemKey = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_books);
        Toolbar toolbar = (Toolbar) findViewById(R.id.shared_books_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bookListAdapter = new BookListAdapter(this, sharedBooks);
        final ListView booksListView = (ListView) findViewById(R.id.shared_books);
        booksListView.setAdapter(bookListAdapter);

        booksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), BookActivity.class);
                intent.putExtra("book", (Book)booksListView.getAdapter().getItem(position));
                startActivity(intent);
            }
        });

        booksListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                switch(booksListView.getId()) {
                    case R.id.shared_books:
                        final int lastItem = firstVisibleItem + visibleItemCount;
                        if(lastItem == totalItemCount && totalItemCount > 0 && lastItemKey != null && lastItemKey != prevLastItemKey) {
                            updateSharedBooksList(lastItemKey);
                            prevLastItemKey = lastItemKey;
                        }
                }
            }
        });

        updateSharedBooksList("");
    }

    private void updateSharedBooksList(String startAt) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference bookDBRef = database.getReference("book");
        if(!sharedBooks.isEmpty()) sharedBooks.remove(sharedBooks.size() -1 );
        bookDBRef.orderByKey().startAt(startAt).limitToFirst(10).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(null != dataSnapshot && dataSnapshot.getValue(Book.class) != null)
                {
                    sharedBooks.add(dataSnapshot.getValue(Book.class));
                    lastItemKey = dataSnapshot.getKey();
                    bookListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
