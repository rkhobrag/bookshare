package com.rkhobrag.bookshare.common;

/**
 * Created by rakesh on 1/7/16.
 */
public class BookShareException extends RuntimeException {
    public BookShareException(String mssg, Exception e) {
        super(mssg, e);
    }

    public BookShareException(String mssg) {
        super(mssg);
    }
}
