package com.rkhobrag.bookshare;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rkhobrag.bookshare.R;

public class MyBooksList extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_my_books_list);

        String[] books = getResources().getStringArray(R.array.my_books);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.my_books_view, books);
        ListView listView = (ListView)findViewById(R.id.my_books_list);
        listView.setAdapter(arrayAdapter);
    }
}


